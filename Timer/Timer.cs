﻿using System;
using System.Threading;

namespace Timer
{
    public class Timer
	{
		/// <summary>
		/// Имя таймера
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Количество тиков в секундах
		/// </summary>
		public int Ticks { get; private set; }

		public Timer(string name, int ticks)
        {
			if (string.IsNullOrEmpty(name))
            {
				throw new ArgumentException("timerName is null", nameof(name));
            }

			if (ticks <= 0)
            {
				throw new ArgumentException("Number of ticks is less than or equal to 0", nameof(ticks));
            }

			Name = name;
			Ticks = ticks;
        }

		public void Start()
        {
			OnStart();

			while (Ticks > 1)
            {
				Thread.Sleep(1000);
				--Ticks;
				OnTick();
            }

			OnStop();
        }

		public event EventHandler<TimerEventArgs> StartedEventHandler = delegate { };

		protected virtual void OnStart()
        {
			StartedEventHandler?.Invoke(this, new TimerEventArgs(Name, Ticks));
        }

		public event EventHandler<TimerEventArgs> TickedEventHandler = delegate { };

		protected virtual void OnTick()
		{
			TickedEventHandler?.Invoke(this, new TimerEventArgs(Name, Ticks));
		}

		public event EventHandler<TimerEventArgs> StoppedEventHandler = delegate { };

		protected virtual void OnStop()
		{
			StoppedEventHandler?.Invoke(this, new TimerEventArgs(Name, Ticks));
		}
    }
}