﻿using System;
using Timer.Interfaces;

namespace Timer.Implementation
{
	public class CountDownNotifier : ICountDownNotifier
	{
		private readonly Timer _timer;

		public CountDownNotifier(Timer timer)
        {
			_timer = timer;
        }

		public void Init(Action<string, int> startDelegate, Action<string> stopDelegate, Action<string, int> tickDelegate)
		{
			_timer.StartedEventHandler += (timerName, timerTicks) => startDelegate?.Invoke(_timer.Name, _timer.Ticks);
			_timer.TickedEventHandler += (timerName, timerTicks) => tickDelegate?.Invoke(_timer.Name, _timer.Ticks);
			_timer.StoppedEventHandler += (timerName, timerTicks) => stopDelegate?.Invoke(_timer.Name);
		}

		public void Run()
		{
			_timer.Start();
		}
	}
}