﻿using System;

namespace Timer
{
    public class TimerEventArgs : EventArgs
    {
        public string TimerName { get; private set; }

        public int TimerTicks { get; private set; }

        public TimerEventArgs(string timerName, int ticks)
        {
            TimerName = timerName;
            TimerTicks = ticks;
        }
    }
}