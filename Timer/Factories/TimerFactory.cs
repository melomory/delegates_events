﻿namespace Timer.Factories
{
	public class TimerFactory
	{
		public Timer CreateTimer(string name, int ticks) => new Timer(name, ticks);
	}
}