﻿using System;
using Timer.Implementation;
using Timer.Interfaces;

namespace Timer.Factories
{
	public class CountDownNotifierFactory
	{
		public ICountDownNotifier CreateNotifierForTimer(Timer timer)
		{
			if (timer is null)
            {
				throw new ArgumentNullException(nameof(timer), "timer is null");
            }

			return new CountDownNotifier(timer);
		}
	}
}
